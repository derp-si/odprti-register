# Odprti Register

[
    ![Datum zadnje posodobitve](
        https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fgitlab.com%2Fderp-si%2Fodprti-register%2F-%2Fpipelines.json&query=pipelines%5B0%5D.created_at&label=Zadnja%20posodobitev
    )
](
    https://gitlab.com/derp-si/odprti-register/pipelines/latest
)
[
    ![Prenos datotek](
        https://img.shields.io/badge/Prenos_datotek-blue?logo=dropbox&labelColor=777
    )
](
    https://gitlab.com/derp-si/odprti-register/-/jobs/artifacts/main/browse/out?job=build_dataset
)

**Odprti register** na enem mestu združuje naslednje registre:

 - Poslovni register Slovenije
 - Seznam davčnih zavezancev - pravne osebe
 - Seznam davčnih zavezancev - fizične osebe, ki opravljajo dejavnost

## Shema podatkovne baze

