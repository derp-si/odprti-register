import os.path

from xml.etree.ElementTree import iterparse

import pandas as pd
from sqlalchemy import create_engine

from tqdm import tqdm
from tqdm.utils import ObjectWrapper

class Wrapper(ObjectWrapper):
    def __init__(self, stream, tqdm):
        super().__init__(stream)
        self.stream = stream
        self.tqdm = tqdm

    def __iter__(self):
        return self._wrapped.__iter__()
    
    def __next__(self):
        l = self._wrapped.__next__()
        self.tqdm.update(len(l))
        return l


FILES = (
    (
        'davcna_fizicne',
        'cache/DURS_zavezanci_DEJ.txt',
        ('davčna_številka','matična_številka','šifra_dejavnosti','ime','naslov','finančni_urad',),
    ), 
    (
        'davcna_pravne',
        'cache/DURS_zavezanci_PO.txt',
        ('f1','f2','davčna_številka','matična_številka','datum_ddv','šifra_dejavnosti','ime','naslov','finančni_urad',),
    ),
)


engine = create_engine('sqlite:///out/podatki.sqlite', echo=False)

for output_name, input_file, columns in FILES:
    with open(input_file) as f:
        with tqdm(unit='B', unit_scale=True, unit_divisor=1024, miniters=1, total=os.path.getsize(input_file)) as t:
            wrapped = Wrapper(f, t)
            df = pd.read_fwf(wrapped, names=columns, index_col=False, infer_nrows=1000)

    print('  Writing SQLite...')
    df.set_index(['davčna_številka', 'matična_številka'])
    df.to_sql(output_name, con=engine, if_exists='replace',)

    print('  Writing CSV...')
    df.to_csv('out/' + output_name + '.csv')
