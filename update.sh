#!/bin/sh

mkdir cache out

cd cache || exit 1

echo
echo DOWNLOAD
echo

aria2c --continue=true --allow-overwrite=true --conditional-get=true -i ../aria.txt

echo
echo DECOMPRESS
echo

for f in *.zip; do
    unzip -o "$f"
done

cd .. || exit 1

echo
echo PRS
echo

python convert_prs.py

echo
echo FURS
echo

python convert_davcna.py

echo 
echo DIAGRAM
echo

sqlite3 -list out/podatki.sqlite < sqlite-schema-diagram.sql > schema.dot
dot -Tsvg -oout/schema.svg schema.dot
rm schema.dot

echo
echo COMPRESS
echo

zstd -f "out/podatki.sqlite"

echo
echo DONE
echo

ls out/
