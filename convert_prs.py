import pandas as pd
from sqlalchemy import create_engine

file_path = r'cache/opsiprs.csv'
rows = []

print(' Reading CSV...')
df_original = pd.read_csv(file_path, encoding='utf-16')  # UTF-16 -- hvala AJPES!

df = pd.DataFrame({
    'matična_številka': df_original['Matična številka'],
    'ime': df_original['Popolno ime'],
    'oblika': df_original['Pravnoorganizacijska oblika'],
    'organ': df_original['Registrski organ'],
    # upravna_enota (ni več v viru)
    # regija (ni več v viru)
    # občina (ni več v viru)
    'pošta': df_original['Pošta'],
    'poštna_številka': df_original['Poštna št '],  # Presledek na koncu -- hvala AJPES! :(
    'naselje': df_original['Naselje'],
    'ulica': df_original['Ulica'],
    'hišna_številka': df_original['Hišna št '],  # ma vi se norca delate...
    'hišna_št_dodatek': df_original['Hišna št  dodatek'],  # DVOJNI presledek -- hvala AJPES! :(((
    # 'država': df_original['Država'],  # ne potrebujemo
    # mid (GURS je ukinil HSE_MID, zdaj se zdaj se uporablja EID_HISNA_STEVILKA)
    'eid_hisna_stevilka': df_original['HSEID'],
})

df.set_index('matična_številka')

engine = create_engine('sqlite:///out/podatki.sqlite', echo=False)

print('  Writing SQLite...')
df.to_sql('prs', con=engine, if_exists='replace')

print('  Writing CSV...')
df.to_csv('out/prs.csv')
